# README #

This is a simple utility project that allows to tune values of the HSV component filter use provided image.
These values are typically in use in computer vision when red components are needed to extract from image.