import cv2
import imutils
import numpy as np
from cv2 import createCLAHE

# Load an image
image = cv2.imread("stop_sign_14.jpg", 1)

# Scale image, don't need to process huge one
height, width = image.shape[:2]
if height > width:
    image = imutils.resize(image, height=480)
else:
    image = imutils.resize(image, width=640)


# A function to use as dummy callback for HSV sliders.
def callback(arg):
    pass


# Normalize colors of the provided RGB image.
def normalize_colors(image):
    # Convert RGB color image to Lab
    image_lab = cv2.cvtColor(image, cv2.COLOR_BGR2Lab)

    # Extract the L channel
    # Now we have the L image in "list[0]"
    list = cv2.split(image_lab)

    # Apply the CLAHE algorithm to the L channel
    clahe = createCLAHE(clipLimit=4.0, tileGridSize=(4, 4))
    dst = clahe.apply(list[0])

    # Merge the the color planes back into an Lab image
    list[0] = dst.copy()
    cv2.merge(list, image_lab)

    # Convert back to RGB
    image_rgb = cv2.cvtColor(image_lab, cv2.COLOR_Lab2BGR)

    # Show Normalized image
    # cv2.imshow("Normalized", image_rgb)

    return image_rgb

window_name = "HSV thresholds"
cv2.namedWindow(window_name)

# Create a set of sliders to tune HSV values
cv2.createTrackbar('LowR-LowMask-H', window_name, 0, 255, callback)
cv2.createTrackbar('LowR-LowMask-S', window_name, 70, 255, callback)
cv2.createTrackbar('LowR-LowMask-V', window_name, 50, 255, callback)
cv2.createTrackbar('UpperR-LowMask-H', window_name, 10, 255, callback)
cv2.createTrackbar('UpperR-LowMask-S', window_name, 255, 255, callback)
cv2.createTrackbar('UpperR-LowMask-V', window_name, 255, 255, callback)

cv2.createTrackbar('LowR-UpperMask-H', window_name, 165, 255, callback)
cv2.createTrackbar('LowR-UpperMask-S', window_name, 70, 255, callback)
cv2.createTrackbar('LowR-UpperMask-V', window_name, 50, 255, callback)
cv2.createTrackbar('UpperR-UpperMask-H', window_name, 180, 255, callback)
cv2.createTrackbar('UpperR-UpperMask-S', window_name, 255, 255, callback)
cv2.createTrackbar('UpperR-UpperMask-V', window_name, 255, 255, callback)

# Normalize image
image = normalize_colors(image)

while True:
    copy_image = image.copy()
    img_hsv = cv2.cvtColor(copy_image, cv2.COLOR_BGR2HSV)

    # Read values of the HSV thresholds from the sliders
    low_LowMask_H = cv2.getTrackbarPos("LowR-LowMask-H", window_name)
    low_LowMask_S = cv2.getTrackbarPos("LowR-LowMask-S", window_name)
    low_LowMask_V = cv2.getTrackbarPos("LowR-LowMask-V", window_name)
    upper_LowMask_H = cv2.getTrackbarPos("UpperR-LowMask-H", window_name)
    upper_LowMask_S = cv2.getTrackbarPos("UpperR-LowMask-S", window_name)
    upper_LowMask_V = cv2.getTrackbarPos("UpperR-LowMask-V", window_name)

    low_UpperMask_H = cv2.getTrackbarPos("LowR-UpperMask-H", window_name)
    low_UpperMask_S = cv2.getTrackbarPos("LowR-UpperMask-S", window_name)
    low_UpperMask_V = cv2.getTrackbarPos("LowR-UpperMask-V", window_name)
    upper_UpperMask_H = cv2.getTrackbarPos("UpperR-UpperMask-H", window_name)
    upper_UpperMask_S = cv2.getTrackbarPos("UpperR-UpperMask-S", window_name)
    upper_UpperMask_V = cv2.getTrackbarPos("UpperR-UpperMask-V", window_name)

    # Lower mask (0-10)
    lower_red = np.array([low_LowMask_H, low_LowMask_S, low_LowMask_V])
    upper_red = np.array([upper_LowMask_H, upper_LowMask_S, upper_LowMask_V])
    mask0 = cv2.inRange(img_hsv, lower_red, upper_red)

    # Upper mask (170-180)
    lower_red = np.array([low_UpperMask_H, low_UpperMask_S, low_UpperMask_V])
    upper_red = np.array([upper_UpperMask_H, upper_UpperMask_S, upper_UpperMask_V])
    mask1 = cv2.inRange(img_hsv, lower_red, upper_red)

    # Join masks
    mask = mask0 | mask1

    # Set output img to zero everywhere except mask
    # Bitwise-AND mask and original image
    copy_image = cv2.bitwise_and(copy_image, copy_image, mask=mask)

    # Show red components
    cv2.imshow("Image", copy_image)

    key = cv2.waitKey(1) & 0xFF

    # If the `q` key was pressed, break from the loop
    if key == ord("q"):
        break

cv2.destroyAllWindows()
